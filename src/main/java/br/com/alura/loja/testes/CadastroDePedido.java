package br.com.alura.loja.testes;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.EntityManager;

import br.com.alura.loja.dao.CategoriaDAO;
import br.com.alura.loja.dao.ClienteDAO;
import br.com.alura.loja.dao.PedidoDAO;
import br.com.alura.loja.dao.ProdutoDAO;
import br.com.alura.loja.modelo.Categoria;
import br.com.alura.loja.modelo.Cliente;
import br.com.alura.loja.modelo.ItemPedido;
import br.com.alura.loja.modelo.Pedido;
import br.com.alura.loja.modelo.Produto;
import br.com.alura.loja.util.JPAUtil;
import br.com.alura.loja.vo.RelatorioDeVendasVo;

public class CadastroDePedido {

	public static void main(String[] args) {

		popularBancoDados();

		EntityManager entityManager = JPAUtil.getEntityManager();

		ProdutoDAO produtoDAO = new ProdutoDAO(entityManager);
		ClienteDAO clienteDAO = new ClienteDAO(entityManager);

		Produto produto = produtoDAO.buscarPorId(1l);
		Produto produto2 = produtoDAO.buscarPorId(2l);
		Produto produto3 = produtoDAO.buscarPorId(3l);

		Cliente cliente = clienteDAO.buscarPorId(1l);

		entityManager.getTransaction().begin();

		Pedido pedido = new Pedido(cliente);
		pedido.adicionarItem(new ItemPedido(10, pedido, produto));
		pedido.adicionarItem(new ItemPedido(40, pedido, produto2));

		Pedido pedido2 = new Pedido(cliente);
		pedido2.adicionarItem(new ItemPedido(2, pedido2, produto3));

		PedidoDAO pedidoDAO = new PedidoDAO(entityManager);
		pedidoDAO.cadastrar(pedido);
		pedidoDAO.cadastrar(pedido2);

		entityManager.getTransaction().commit();

		BigDecimal totalVendido = pedidoDAO.valorTotalVendido();
		System.out.println("Valor total: " + totalVendido);

		List<RelatorioDeVendasVo> relatorio = pedidoDAO.relatorioDeVendas();
		relatorio.forEach(System.out::println);

	}

	private static void popularBancoDados() {

		Categoria celulares = new Categoria("CELULARES");
		Categoria videogames = new Categoria("VIDEOGAMES");
		Categoria informatica = new Categoria("INFORMATICA");

		Produto celular = new Produto("Xiaomi POCOX3", "Melhor custo/benef�cio do momento.", new BigDecimal("1000"),
				celulares);
		Produto videogame = new Produto("PS5", "Playstation 5", new BigDecimal("5000"), videogames);
		Produto macbook = new Produto("MacBook", "MacBook PRO", new BigDecimal("10000"), informatica);

		Cliente cliente = new Cliente("Rubens", "123456");

		EntityManager entityManager = JPAUtil.getEntityManager();
		ProdutoDAO produtoDAO = new ProdutoDAO(entityManager);
		CategoriaDAO categoriaDAO = new CategoriaDAO(entityManager);
		ClienteDAO clienteDAO = new ClienteDAO(entityManager);

		entityManager.getTransaction().begin();

		categoriaDAO.cadastrar(celulares);
		categoriaDAO.cadastrar(videogames);
		categoriaDAO.cadastrar(informatica);

		produtoDAO.cadastrar(celular);
		produtoDAO.cadastrar(videogame);
		produtoDAO.cadastrar(macbook);

		clienteDAO.cadastrar(cliente);

		entityManager.getTransaction().commit();
		entityManager.close();
	}

}
